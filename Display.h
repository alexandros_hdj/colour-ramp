//
//  Display.h
//  Gradient
//
//  Created by Alexandros Hadjigeorgiou on 18/08/2015.
//  Copyright (c) 2015 Alexandros Hadjigeorgiou. All rights reserved.
//

#ifndef _Display_h
#define _Display_h

class Display {
    public:
        Display();
        bool connect(const char *display_name);
        void get_size(int &width, int &height);
        void draw_raster(int x, int y, const unsigned short *pixels, int width);
		void draw_colours();
        ~Display();
};

#endif