//	Colour Ramp
//
//  Display.cpp
//  Gradient
//
//  Created by Alexandros Hadjigeorgiou on 18/08/2015.
//
//	

#include <windows.h>
#include <cstring>
#include <cmath>
#include <cstdio>
#include <cassert>
#include <memory>
#include "Display.h"

#define W 16
#define H 9
static unsigned short frame_buffer[W*H];

Display::Display()
{
    memset(frame_buffer, 0, sizeof(frame_buffer));
}

Display::~Display()
{
    unsigned short *pix = frame_buffer;
    
    for (int y = 0; y < H; y++) {
        for (int x = 0; x < W; x++) {
            if (x > 0) {
                printf(" ");
            }
            printf("%04X", *pix++);
        }
        printf("\n");
    }
}

bool Display::connect(const char *display_name)
{ return true; }

void Display::get_size(int &width, int &height)
{
    width = W;
    height = H;
}

//Changes an element in the memory at the appropriate coordinates
void Display::draw_raster(int x, int y,	const unsigned short *pixels, int width)
{
    memcpy(&frame_buffer[y*W + x], pixels, width*sizeof(unsigned short));
}

//draw_colours method, uncommenting this provides 
//LNK2019 errors on the GetDC, setPixel and ReleaseDC functions.
void Display::draw_colours()
{
	/*//Get a console handle
	HWND myconsole = GetConsoleWindow();
	//Get a handle to device context
	HDC mydc = GetDC(myconsole);

	unsigned short *pix = frame_buffer;

	WORD red_mask = 0xF800;
	WORD green_mask = 0x7E0;
	WORD blue_mask = 0x1F;

	//Draw pixels
	for (int y = 0; y < H; y++) {
		for (int x = 0; x < W; x++) {

			BYTE red_value = (*pix & red_mask) >> 11;
			BYTE green_value = (*pix & green_mask) >> 5;
			BYTE blue_value = (*pix & blue_mask);

			//Choose any color
			COLORREF COLOR = RGB((COLORREF)red_value, (COLORREF)green_value, 
				(COLORREF)blue_value);

			SetPixel(mydc, x, y, COLOR);
			*pix++;
		}
	}
	ReleaseDC(myconsole, mydc);*/
}