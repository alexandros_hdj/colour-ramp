==============================================================
Author: Alexandros Hadjigeorgiou
Email: alex_hdjgrg@hotmail.com
==============================================================

The program is compiled with the following command on command prompt when in the same folder:
cl /EHsc Display.cpp main.cpp -o ramp

This creates the ramp.exe which works as specified in the exercise, it requires 3-5 inputs including the name of the Display and 2-4 colour values. 
And it will run and output the hex values of the 16x9 memory elements.

The values provided are assigned to the four ends of the memory array.
Uses bilinear interpolation each element is calculated and a gradient is calculated. 
The formula used for it first normalises the location of the point horizontally.
Then using the values provided for the four corners finds the relative values for top and bottom.
Using these values and normalising the vertical path the desired element value is found. 

Known problems:
The SetPixel, GetDC, ReleaseDC create LNK2019 errors unresolved external symbol.
This usually is associated with variables declared wrongly or not instantiated properly.
Could not identify the source

The input arguments placed on the command line are directly detected as Hex values.
Ideally the program should identify between hex and dec values. 
Could not find the correct command to use to detect whether the arg[i] was hex or dec.