//	Colour Ramp file
//
//  main.cpp
//  Gradient
//
//  Created by Alexandros Hadjigeorgiou on 18/08/2015.

#include <windows.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include "Display.h"

using namespace std;

int main(int argc, const char *arg[]) {

	Display* dis = new Display();
	
	//Detects the number of arguments required for the system
	if (argc>1) {
		const char *display_name = arg[1];
		if (dis->connect(display_name)) {
			std::cout << "Display Connected is: " << display_name << '\n';
		}
	} else {
		std::cerr << "Please insert display argument\n";
		return 0;
	}

	int width;
	int height;
	int x[4];
	dis->get_size(width, height);
	std::cout << "\n" << width << " and " << height << '\n';

	//Values provided are turned to appropriate type and detect hex values
	if (argc > 3 && argc < 7) {
		for (int i = 2; i<argc; i++) {
			std::istringstream ss(arg[i]);
			
			ss >> std::hex; // implements the hexadecimal stream
			//discovered a bug here that it changes dec values to hex as well
			//need to provide an if statement such as isxdigit (although this one doesn't work)
			//ss >> std::dec will change the stream to decimal
		
			//error handling here incomplete as it can be fooled if the first digits are valid.
			if (!(ss >> x[i - 2])) {
				std::cerr << "Invalid argument " << i << ": " << arg[i] << '\n' << "Value needs to be of hexadecimal or integer type\n";
				return 0;
			}
		}
	}
	else {
		std::cerr << "Too many or too few arguments, the program needs at least two colours to run\n" << "The arguments are in the format: display, top_left, top_right, bottom_left, bottom_right";
		return 0;
	}

	//initialise the variables on the four corners
	unsigned short tl = (unsigned short)x[0];
	unsigned short tr = (unsigned short)x[1];
	unsigned short bl = (unsigned short)x[0];
	unsigned short br = (unsigned short)x[1];

	if (argc>4)
		bl = (unsigned short)x[2];
	if (argc>5)
		br = (unsigned short)x[3];

	//float is used because int would not be accurate enough for values that are close together.
	float colour = (float)tl;

	//the for loop calculates the appropriate hex value for each memory element.
	for (int i = 0; i < width; i++) {
		float top_width = (float)i / (width - 1)*tr + (float)(width - i - 1) / (width - 1)*tl;
		float bottom_width = (float)i / (width - 1)*br + (float)(width - i - 1) / (width - 1)*bl;
		for (int k = 0; k < height; k++) {
			colour = (float)k / (height - 1)*bottom_width + (float)(height - k - 1) / (height - 1)*top_width;
			const unsigned short colours[] = { (unsigned short)colour };
			dis->draw_raster(i, k, colours, 1);
		}
	}

	//draw_colours is meant to derive the colour from each memory element and output a series of pixels 
	//dis->draw_colours();

	delete dis;
}